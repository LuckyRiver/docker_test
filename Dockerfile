# we are extending everything from tomcat image ...
FROM fabric8/tomcat-7
EXPOSE 8080
RUN rm -fr /usr/local/tomcat/webapps/ROOT
MAINTAINER LuckyRiver

COPY wallet-service-rest.war /opt/apache-tomcat-7.0.90/webapps

COPY ./wallet-service-rest.xml /opt/apache-tomcat-7.0.90/conf/Catalina/localhost/

COPY ./server.xml /opt/apache-tomcat-7.0.90/conf

EXPOSE 8080

CMD chmod +x /usr/local/tomcat/bin/catalina.sh

CMD ["catalina.sh", "run"]
